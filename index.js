function guardarEnLocalStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var txtValor = document.getElementById("txtValor"); /* Referencia al input de valor */
  var clave = txtClave.value;
  var valor = txtValor.value;
  //localStorage.setItem(clave, valor);
  sessionStorage.setItem(clave, valor);
  var objeto = {
    nombre:"Ezequiel",
    apellidos:"Llarena Borges",
    ciudad:"Madrid",
    pais:"España"
  };
  //localStorage.setItem("json", JSON.stringify(objeto));
}
function leerDeLocalStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var clave = txtClave.value;
  //var valor = localStorage.getItem(clave);
  var valor = sessionStorage.getItem(clave);
  var spanValor = document.getElementById("spanValor");
  spanValor.innerText = valor;
  var datosUsuario = JSON.parse(localStorage.getItem("json"));
  console.log(datosUsuario.nombre);
  console.log(datosUsuario.pais);
  console.log(datosUsuario);
}

function eliminaEnLocalStorage(){
  var clave = document.getElementById("txtClave").value;
  alert(clave);
  //localStorage.removeItem(clave);
  sessionStorage.removeItem(clave);
}

function limpiaEnLocalStorage(){
  //localStorage.clear();
  sessionStorage.clear();
}

function nroElementosEnLocalStorage(){
  //var valor = localStorage.length;
  var valor = sessionStorage.length;
  var spanValor = document.getElementById("spanValor");
  spanValor.innerText = valor;
}